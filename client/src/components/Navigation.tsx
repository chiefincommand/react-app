import * as React from 'react';
import {Link} from "react-router-dom";
import {ReactPropTypes} from "react";
import {PubSubService} from "../services/PubSubService";


interface State {
    searchText: string
}

class Navigation extends React.Component<any, State> {

    constructor(props : ReactPropTypes) {
        super(props);

        this.state = {
            searchText: ''
        };

        this.onSearchTermChange = this.onSearchTermChange.bind(this);
    }

    onSearchTermChange(e: React.FormEvent<EventTarget>): void {
        let target = e.target as HTMLInputElement;
        this.setState({searchText: target.value});
        PubSubService.getInstance().fire('dialog:open', {
            title: 'Oops',
            subtitle: 'Sorry this isn\'t implemented yet',
            actions: [{
                value: 'Oh damn',
                id: 0,
                onClick: () => {
                    PubSubService.getInstance().fire('dialog:close');
                }
            }]
        })
    }

    render() {
        return (
            <nav className="app-navigation">
                <input type="text" value={this.state.searchText} onChange={this.onSearchTermChange}/>
                <Link className="app-nav-link" to="/">Home</Link>
                <Link className="app-nav-link" to="/browse">Browse</Link>
            </nav>
        );
    }
}

export {Navigation};