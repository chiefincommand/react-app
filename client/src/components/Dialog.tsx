import * as React from "react";
import {DialogAction, DialogContent} from "../interfaces/DialogData";


class Dialog extends React.Component<DialogContent> {
    // static defaultProps = {
    //     actions: [{
    //         value: 'OK',
    //         id: 0,
    //         onClick: () => {
    //             PubSubService.getInstance().fire('dialog:close');
    //         }
    //     }]
    // };

    renderAction(action: DialogAction) {
        return <button key={action.id} onClick={() => action.onClick()}>{action.value}</button>;
    }

    renderActions() {
        if (this.props.actions) {
            return this.props.actions.map(this.renderAction);
        }
        return '';

    }

    render() {
        return (
            <div className="app-dialog-container">
                <div className="app-dialog">
                    <h2>{this.props.title}</h2>
                    <h3>{this.props.subtitle}</h3>
                    {this.renderActions()}
                </div>
                <div className="app-dialog-backdrop"></div>
            </div>
        );
    }
}


export {Dialog};