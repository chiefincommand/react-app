import * as React from 'react';
import {Tile} from "./Tile";
import {offerData} from '../offers';
import {Link} from "react-router-dom";

class Browse extends React.Component {

    render() {
        return (
          <div className="app-browse">
              <h1>Browse</h1>
              <section className="app-browse-tilecontainer">
                  {
                      offerData.offers.map((offer) => {
                          return (<Link to={offer.gdpPath}><Tile key={offer.offerId} offer={offer} /></Link>);
                      })
                  }
              </section>
          </div>
        );
    }
}

export {Browse};