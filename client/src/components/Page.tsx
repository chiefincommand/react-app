import {Home} from "./Home";
import * as React from "react";
import {Browse} from "./Browse";
import {Gdp} from "./Gdp";
import {Route, Switch} from "react-router";


const FourOFour = () => <div>Not sure what you are looking for mate!</div>;

class Page extends React.Component {

    componentDidMount() { //window is available/ can bind events / call rest api

    }

    componentWillUnmount() { //clean up

    }

    render() {
        return (
            <section className="app-page">
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/browse" component={Browse}/>
                    <Route exact path="/:franchise/:game" component={Gdp}/>
                    <Route component={FourOFour}/>
                </Switch>
            </section>
        );
    }
}

export {Page};

