import * as React from "react";
import {CarouselItem} from "../interfaces/CarouselData";
import {CarouselMedia} from "./CarouselMedia";

type Props = {
    items: CarouselItem[]
}

type State = {
    activeIndex: number,
    showCarouselMedia: boolean
};

type Event = {
    preventDefault: Function
};

class Carousel extends React.Component<Props, State> {

    onPrevClick = this.handlePrevClick.bind(this);

    onNextClick = this.handleNextClick.bind(this);

    onItemClick = this.handleSelectedItem.bind(this);

    state: State = {
        activeIndex: 0,
        showCarouselMedia: false
    };

    componentDidMount() {
        // this.setState({activeIndex: 0});
    }

    // static getDerivedStateFromProps(): Props {
    //     return {
    //         items: []
    //     };
    // }

    handleNextClick(event: Event) {
        event.preventDefault();
        const activeIndex = this.state.activeIndex;
        if (activeIndex + 1 > this.props.items.length - 1) {
            this.setState({activeIndex: 0});
        } else {
            this.setState({activeIndex: activeIndex + 1})
        }
    }

    handlePrevClick(event: Event) {
        event.preventDefault();
        const activeIndex = this.state.activeIndex;
        if (activeIndex - 1 < 0) {
            this.setState({activeIndex: this.props.items.length - 1});
        } else {
            this.setState({activeIndex: activeIndex - 1})
        }
    }

    handleMediaClick() {
        this.setState({showCarouselMedia : !this.state.showCarouselMedia});
    }

    handleSelectedItem(e: React.MouseEvent<EventTarget>) {
        let target = e.target as HTMLElement;

        if (target.dataset.index) {
            this.setState({activeIndex: +target.dataset.index}, this.handleMediaClick);
        }
    }



    displayCarouseItems() {

        const carouselItems = this.props.items.map((item: CarouselItem, index: number) => {
            return <li key={item.id} onClick={this.onItemClick}
                       className={`app-carousel-item  ${index === this.state.activeIndex ? 'app-carousel-item-active' : ''}`}>
                <img src={item.image} data-index={index}/>
            </li>
        });


        return [<li key="-2" className="app-carousel-item-prev"><a href="" onClick={this.onPrevClick}>Prev</a></li>,
            ...carouselItems,
            <li key="-5" className="app-carousel-item-next"><a href="" onClick={this.onNextClick}>Next</a></li>];


    }

    render() {
        //console.log(this.state.items);
        return (
            <div className="app-carousel">
                <ul className="app-carousel-container">
                    {this.displayCarouseItems()}
                </ul>
                {this.state.showCarouselMedia? <CarouselMedia {...this.props.items[this.state.activeIndex]} onDismiss={() => this.handleMediaClick()} /> : ''}
            </div>
        );
    }
}

export {Carousel};