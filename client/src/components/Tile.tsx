import * as React from "react";

const IMAGE_SERVER = 'https://originassets.akamaized.net/origin-com-store-final-assets-prod/';

interface Props {
    offer: any
}

class Tile extends React.Component<Props> {

    render() {
        const offer = this.props.offer;
        return (
            <section className="app-tile">
                <h3 className="app-tile-title">{offer.i18n.displayName}</h3>
                <div className="app-tile-body">

                    {/*<p dangerouslySetInnerHTML={{__html: offer.i18n.shortDescription}}></p>*/}
                    <img src={`${IMAGE_SERVER}${offer.i18n.packArtLarge}`}/>
                </div>
            </section>
        );
    }
}

export {Tile};
