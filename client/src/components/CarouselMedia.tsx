import * as React from "react";
import {CarouselItem} from "../interfaces/CarouselData";


class CarouselMedia extends React.Component<CarouselItem> {

    onBackdropClick = this.handleBackdropClick.bind(this);

    handleBackdropClick() {
        if (typeof this.props.onDismiss === 'function') {
            this.props.onDismiss();
        }
    }

    render() {
        return (
            <div className="app-carousel-media">
                <div className="app-carousel-media-content">
                    <h3>{this.props.title}</h3>
                    <img src={this.props.image}/>
                    <h4>{this.props.subtitle}</h4>
                </div>
                <div className="app-dialog-backdrop" onClick={this.onBackdropClick}>&nbsp;</div>
            </div>
        );
    }
}

export {CarouselMedia};