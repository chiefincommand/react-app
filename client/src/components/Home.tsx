import * as React from 'react';
import {Carousel} from "./Carousel";
import {carouselData} from '../offers';

class Home extends React.Component {

    render() {
        return (
            <section>
                <header>Home</header>
                {carouselData.items.length > 0 ? <Carousel items={carouselData.items} /> : ''}
            </section>
        );
    }
}

export {Home};