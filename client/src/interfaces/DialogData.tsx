interface DialogAction {
    value: string,
    id: number,
    onClick: Function
}

interface DialogContent {
    title?: string,
    subtitle?: string,
    actions?: DialogAction[]
}

export {DialogContent, DialogAction};