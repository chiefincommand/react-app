interface CarouselItem {
    id: number,
    image: string,
    title?: string,
    subtitle?: string,
    onDismiss?: Function
}


export {CarouselItem};