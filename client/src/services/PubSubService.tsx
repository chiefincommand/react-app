interface PubSubInterface {
    fire: Function,
    on: Function,
    off: Function
}


type Event = {
    name: string,
    callbacks: Function[]
}


class PubSubService implements PubSubInterface {

    private static instance : PubSubService;

    private constructor(private events: Event[] = []) {
    }

    private findEvent(name: string): Event | undefined {
        if (this.events && this.events.length) {
            return this.events.find((event: Event) => event.name === name);
        }
        return undefined;
    }

    private findEventByIndex(name: string): number {
        if (this.events && this.events.length) {
            return this.events.findIndex((event: Event) => event.name === name);
        }
        return -1;
    }

    fire(name: string, eventData?: any) {
        const event = this.findEvent(name);
        if (event && event.callbacks.length) {
            event.callbacks.forEach((callback: Function) => {
                callback(eventData);
            })
        }
    }

    on(name: string, callback: Function) {
        const event = this.findEvent(name);
        if (event) {
            event.callbacks.push(callback);
        } else {
            this.events.push({
                name,
                callbacks: [callback]
            });
        }
    }

    off(name: string, callback: Function) {
        const event = this.findEvent(name);

        if (event) {
            if (event.callbacks.length === 0 || typeof callback === 'undefined') {
                const index = this.findEventByIndex(name);
                this.events.splice(index, 1);
            }

            const index = event.callbacks.findIndex((callbackFn) => {
                return callback === callbackFn;
            });

            event.callbacks.splice(index, 1);
        }
    }

    static getInstance(): PubSubService {
        if (PubSubService.instance) {
            return PubSubService.instance;
        }
        PubSubService.instance = new PubSubService();
        return PubSubService.instance;
    }

}

export {PubSubService};