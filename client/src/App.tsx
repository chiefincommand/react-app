import * as React from 'react';
import './App.css';
import {Navigation} from "./components/Navigation";
import {Page} from "./components/Page";
import {BrowserRouter} from "react-router-dom";
import {PubSubService} from "./services/PubSubService";
import {Dialog} from "./components/Dialog";
import {DialogContent} from "./interfaces/DialogData";


type State = {
    showDialog: boolean,
    dialogContent?: DialogContent | undefined | null
}

class App extends React.Component<any, State> {

    private pubsubInstance: PubSubService;

    constructor(props: any) {
        super(props);
        this.pubsubInstance = PubSubService.getInstance();
        this.state = {showDialog: false}
    }

    componentDidMount() { //window is available/ can bind events / call rest api

        this.pubsubInstance.on('dialog:open', (event: any) => {
            console.log(event);
            this.setState({showDialog: true, dialogContent: event});
        });

        this.pubsubInstance.on('dialog:close', (event: any) => {
            this.setState({showDialog: false, dialogContent: event});
        });
    }

    public render() {
        return (
            <div className="app">
                <BrowserRouter>
                    <div className="app-container">
                        <Navigation/>
                        <Page/>
                    </div>
                </BrowserRouter>
                {this.state.showDialog ? <Dialog {...this.state.dialogContent} /> : ''}
            </div>
        );
    }
}

export default App;
