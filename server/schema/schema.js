const graphql = require('graphql');
const language = require('graphql/language');
const fetch = require('node-fetch');

const {
    GraphQLObjectType,
    // GraphQLInt,
    GraphQLString,
    // GraphQLID,
    GraphQLList,
    //  GraphQLSchema,
    GraphQLBoolean,
    GraphQLScalarType
    // GraphQLNonNull,
} = graphql;

const {Kind} = language;

function toObject(value) {
    if(typeof value === 'object') {
        return value;
    }
    if(typeof value === 'string' && value.charAt(0) === '{') {
        return Json5.parse(value);
    }
    return null;
}

function parseAst(ast) {
    switch (ast.kind) {
        case Kind.STRING:
        case Kind.BOOLEAN:
            return ast.value;
        case Kind.INT:
        case Kind.FLOAT:
            return parseFloat(ast.value);
        case Kind.OBJECT:
            return parseObject(ast);
        case Kind.LIST:
            return ast.values.map(parseAst);
        default:
            return null;
    }
}

function parseObject(ast) {
    const value = Object.create(null);
    ast.fields.forEach((field) => {
        value[field.name.value] = parseAst(field.value);
    });
    return value;
}

const SiblingType = new GraphQLScalarType({
    name: 'sibling',
    description: "Represents an arbitrary object.",
    parseValue: toObject,
    serialize: toObject,
    parseLiteral: (ast) => {
        console.log(ast);
        switch (ast.kind) {
            case Kind.OBJECT:
                return parseObject(ast);

        }
        return null;
    }
});


const GamehubType = new GraphQLObjectType({
    name: 'gamehub',
    fields: () => ({
        children: {type: SiblingType},
        components: {
            type: new GraphQLObjectType({
                name: 'items',
                fields: () => ({
                    items: {type: GraphQLList(SiblingType)}
                })
            })
        },
        siblings: {type: SiblingType}
    })
});

const OcdType = new GraphQLObjectType({
    name: 'ocd',
    fields: () => ({
        gamehub: {type: GamehubType}
    })
});

const I18nType = new GraphQLObjectType({
    name: 'i18n',
    fields: () => ({
        displayName: {type: GraphQLString},
        longDescription: {type: GraphQLString},
        packArtLarge: {type: GraphQLString}
    })
});

const PlatformType = new GraphQLObjectType({
    name: 'platform',
    fields: () => ({
        platform: {type: GraphQLString},
        multiPlayerId: {type: GraphQLString},
        downloadPackageType: {type: GraphQLString},
        releaseDate: {type: GraphQLString},
        downloadStartDate: {type: GraphQLString},
        useEndDate: {type: GraphQLString},
        executePathOverride: {type: GraphQLString},
        achievementSetOverride: {type: GraphQLString},
        showSubsSaveGameWarning: {type: GraphQLBoolean},
        commerceProfile: {type: GraphQLString},
        originSubscriptionUseEndDate: {type: GraphQLString},
        originSubscriptionUnlockDate: {type: GraphQLString}
    })
});

const CountryType = new GraphQLObjectType({
    name: 'country',
    fields: () => ({
        isPurchasable: {type: GraphQLString},
        catalogPrice: {type: GraphQLString},
        countryCurrency: {type: GraphQLString},
        catalogPriceA: {type: GraphQLList(GraphQLString)},
        countryCurrencyA: {type: GraphQLList(GraphQLString)},
        hasSubscriberDiscount: {type: GraphQLString},
        isPublished: {type: GraphQLBoolean},
        isAvailableForPreview: {type: GraphQLBoolean},
        giftable: {type: GraphQLString},
        bannedCountries: {type: GraphQLList(GraphQLString)}
    })

});


const OfferType = new GraphQLObjectType({
    name: 'Offer',
    fields: () => ({
        offerId: {type: GraphQLString},
        imageServer: {type: GraphQLString},
        i18n: {type: I18nType},
        gdpPath: {type: GraphQLString},
        offerPath:  {type: GraphQLString},
        offerType: {type: GraphQLString},
        isDownloadable: {type: GraphQLString},
        gameDistributionSubType: {type: GraphQLString},
        trialLaunchDuration: {type: GraphQLString},
        masterTitleId: {type: GraphQLString},
        alternateMasterTitleIds: {type: GraphQLList(GraphQLString)},
        suppressedOfferIds: {type: GraphQLList(GraphQLString)},
        originDisplayType: {type: GraphQLString},
        platforms: {type: GraphQLList(PlatformType)},
        ratingSystemIcon: {type: GraphQLString},
        gameRatingUrl: {type: GraphQLString},
        gameRatingPendingMature: {type: GraphQLString},
        gameRatingDescriptionLong: {type: GraphQLList(GraphQLString)},
        gameRatingTypeValue: {type: GraphQLList(GraphQLString)},
        gameRatingDesc: {type: GraphQLList(GraphQLString)},
        countries: {type: CountryType},
        ocd: {
            type: OcdType,
            resolve(parent, args /*same as above args*/) {
                return fetch(`https://data3.origin.com/ocd${parent.offerPath}.en-us.can.ocd`)
                    .then((response) => response.json());
            }
        }
    })
});


module.exports = {
    OfferType,
    OcdType
};