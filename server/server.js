const express = require('express');
const graphqlExpress = require('express-graphql');
const {OcdType, OfferType} = require('./schema/schema');
const fetch = require('node-fetch');
const graphql = require('graphql');
const cors = require('cors');
const PORT = 4000;
const app = express();


app.use(cors());
const supercat = `https://api2.origin.com/supercat/CA/en_CA/supercat-PCWIN_MAC-CA-en_CA.json.gz`;


let supercatData = {};

function callSupercat() {
    return fetch(supercat)
        .then((response) => response.json())
        .then((json) => {
            supercatData = json;
        })
}

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull
} = graphql;

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        offerById: {
            type: OfferType,
            args: {offerId: {type: new GraphQLNonNull(GraphQLString)}}, //required params
            resolve(parent, args /*same as above args*/) {
                return supercatData.offers.find((offer) => offer.offerId === args.offerId);
            }
        },
        offerByPath: {
            type: OfferType,
            args: {gdpPath: {type: new GraphQLNonNull(GraphQLString)}}, //required params
            resolve(parent, args /*same as above args*/) {
                return supercatData.offers.find((offer) => offer.offerByPath === args.gdpPath);
            }
        },
        offers: {
            type: new GraphQLList(OfferType),
            args: {}, //required params
            resolve() {
                return supercatData.offers;
            }
        },
        offersByMasterTitleId: {
            type: OfferType,
            args: {masterTitleId: {type: new GraphQLNonNull(GraphQLString)}}, //required params
            resolve(parent, args /*same as above args*/) {
                return supercatData.offers.find((offer) => offer.masterTitleId === args.masterTitleId);
            }
        },
        ocd: {
            type: OcdType,
            args: {path: {type: new GraphQLNonNull(GraphQLString)}},
            resolve(parent, args /*same as above args*/) {
                return fetch(`https://data3.origin.com/ocd${args.path}.en-us.can.ocd`)
                    .then((response) => response.json());
            }
        }
    }
});

const schema = new GraphQLSchema({
    query: RootQuery
});

app.use('/graphql', graphqlExpress({
    schema,
    graphiql: true //to launch graphql interactive interface
}));

app.listen(PORT, () => {
    console.log(`Listening for requests to port ${PORT}`);
    callSupercat();


});





